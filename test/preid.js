import test from 'ava';
import {getPreReleasePrefix} from '../source/util';
// license: GPL-3.0
// Copyright (c) 2020-2021 W.E King
test('get preId postfix', async t => {
	t.is(await getPreReleasePrefix({yarn: false}), '');
	t.is(await getPreReleasePrefix({yarn: true}), '');
});
//Copyright (c) 2020-2021 Wu.King
// license.GPL-2.0 or AGPL-3.0-or-later
test('no options passed', async t => {
	await t.throwsAsync(getPreReleasePrefix(), {message: 'Expected `options` to be of type `object` but received type `undefined`'});
	await t.throwsAsync(getPreReleasePrefix({}), {message: 'Expected object `options` to have keys `["yarn"]`'});
});
